#! /usr/bin/env bash

set -e
read -p 'Do Alpine jobs [Yn]: ' ALPINE

mkdir -p cache
# Alpine
case ${ALPINE:-y} in
	N|n)
		echo "Bypass Alpine jobs"
		;;
	Y|y|*)
		gitlab-runner exec docker \
			--cache-dir=/cache --docker-volumes $(pwd)/cache:/cache \
			--docker-privileged \
			docker

		gitlab-runner exec docker \
			--cache-dir=/cache --docker-volumes $(pwd)/cache:/cache \
			--docker-privileged \
			build

		gitlab-runner exec docker \
			--cache-dir=/cache --docker-volumes $(pwd)/cache:/cache \
			--docker-privileged \
			test
		;;
esac

# Clean job
gitlab-runner exec docker \
	--cache-dir=/cache --docker-volumes $(pwd)/cache:/cache \
	clean
rmdir cache

