# Introduction

My custom package for alpine.  
. assimp
. qt5-3d

Been on archlinux, I use docker to build/test packages.

# HowTo

First I build a minimal Alpine image using:  
```sh
make docker
```
Then I build package using:  
```sh
make package
```
# Annexes
wiki:  
[Dockerfile Best Practices](https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/)  
[Creating an Alpine package](https://wiki.alpinelinux.org/wiki/Creating_an_Alpine_package)  
[APKBUILD](https://wiki.alpinelinux.org/wiki/APKBUILD_Reference)  

## Assimp

Samples:  
[PKBUILD assimp](https://git.archlinux.org/svntogit/packages.git/tree/trunk/PKGBUILD?h=packages/assimp)  

## Qt5-3d

Qt5:  
[submodules](http://download.qt.io/official_releases/qt/5.9/5.9.2/submodules/)  
Samples:  
[APKBUILD qt5-multimedia](https://git.alpinelinux.org/cgit/aports/tree/community/qt5-qtmultimedia/APKBUILD?id=3b9559d03d093bea6a43ab1d59b87f861737e307)  

