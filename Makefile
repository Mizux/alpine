help:
	@echo "usage:"
	@echo "make docker: generate docker images"
	@echo "make package: build package(s)"
	@echo "make test: run unit tests"
	@echo "make clean: remove build directories"
	@echo "make mrproprer: clean and also remove docker images"

.PHONY: all help clean mrproper docker packages test debug \

all: | mrproper docker package

UID := $(shell id -u)
GID := $(shell id -g)

# DOCKER
docker: alpine.tar

%.tar: docker/%
	docker rmi -f project:$*
	docker build -t project:$* $<
	docker save project:$* -o $@

# PACKAGE
# abuild options:
# -c colored output
# -r Install missing dependencies from system repository (using sudo)
# -R Recursively build and install missing dependencies (using sudo)
# -P Set REPODEST as the repository location for created packages
packages: | pkg-assimp #pkg-qt5-3d
pkg-%: alpine.tar out FORCE
	docker load -i $<
	docker run --rm \
		-v ${PWD}/aports:/aports \
		-v ${PWD}/apkg:/apkg \
		-v ${PWD}/out:/out \
		project:alpine \
		sh -c "sudo apk update && cp /apkg/$*/APKBUILD . && abuild -c -r && sudo cp -r packages/ /out"
out:
	mkdir -p out

# Debug
# --user ${UID}:${GID}
debug: alpine.tar aports out
	docker load -i $<
	docker run --rm --init -ti \
		-v ${PWD}/aports:/aports \
		-v ${PWD}/apkg:/apkg \
		-v ${PWD}/out:/out \
		project:alpine \
		sh -c "sudo apk update && sh"
aports:
	git clone --depth 1 git://git.alpinelinux.org/aports

# TEST
test: test-assimp test-qt5-3d
test-%: alpine.tar FORCE
	docker load -i $<
	docker run --rm \
		-v ${PWD}/out:/out \
		-w /out \
		project:alpine \
		sh -c "sudo apk add --allow-untrusted $*.apk"

# CLEAN
clean:
	rm -rf out

# MRPROPER
mrproper: clean
	docker rmi -f project:alpine
	rm -f alpine.tar
	docker rm $(docker ps --all --filter="status=exited" --quiet)
	docker rmi -f $(docker images --filter="dangling=true" --quiet)

# Any target whose has this prerequisite will be considered Out Of Date.
# like if target was in .PHONY (no always possible for target with pattern).
FORCE:

