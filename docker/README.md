# Dockerfile for Alpine Package Development Environment

From [alpine:latest](https://hub.docker.com/r/alpine/)
* [alpine-sdk](https://pkgs.alpinelinux.org/package/edge/main/x86_64/alpine-sdk)

## Manual Build
You can build an image using:
```sh
cd docker/<distro>
docker build -t project:<distro> .
```

## Run
Then you can test the image using:
```sh
docker run --init --rm -it project:<distro> bash
```

## Annexes
To test CI job locally:
```sh
make package
```
